# Review of the HTML / CSS Page, task #3

- Setting content inside the section for me it is good idea, though i think that first h1 should also be inside it's own div.
- div with class container can stay like this although i would make it with 'display:flex' since it is much easier to organize elements inside.
- image in my opinion should be designed to be background for the div with class inner, there is easy way to implement this.
- Therefore if this is displayed as flex all these items do not require positioning like this as with flex it's much cleaner to organize things like a row or column.
- div with class box is not required if we are doing this with flex.
- paragraphs and headings inside div with class box do not need additional centering since in the body we already assigned that, that also may not be good idea since we need to overwrite it every time.
- button should not contain negative positions, that is old technique for positioning things, nowadays we have much more cleaner approach (with flex we would assign container div) 