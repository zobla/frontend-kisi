const capitalize = (string) => {
    return string
        .split(' ')
        .map(function(word) {
            return word[0].toUpperCase() + word.substr(1);
        })
        .join(' ');
};

console.log(capitalize("hello world"));